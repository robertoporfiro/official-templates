import re
import sys
import glob
import json
import logging
import subprocess

import warnings
from email import utils
from datetime import datetime
from typing import List, Optional
from dataclasses import dataclass, asdict
from concurrent.futures.thread import ThreadPoolExecutor

import yaml
import cerberus
import requests
from colorlog.colorlog import ColoredFormatter
from requests.exceptions import HTTPError

def logger_config():
    """
        Setup the logging environment
    """

    clogger = logging.getLogger()
    format_str = '%(log_color)s%(levelname)s: %(message)s'
    date_format = '%Y-%m-%d %H:%M:%S'
    colors = {'DEBUG': 'green',
              'INFO': 'blue',
              'WARNING': 'bold_yellow',
              'ERROR': 'bold_red',
              'CRITICAL': 'bold_purple'}
    formatter = ColoredFormatter(format_str, date_format, log_colors=colors)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    clogger.addHandler(stream_handler)
    clogger.setLevel(logging.INFO)
    return clogger


logger = logger_config()

LOCAL_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': False},
    'description': {'type': 'string', 'required': False},
    'repositoryPath': {'type': 'string', 'required': True},
})

@dataclass
class Failures:
    criticals: List[str]
    warnings: List[str]
    skipped: List[str]


@dataclass
class ProcessMetadata:
    file: str
    valid: bool
    failures: Failures
    errors: List[str]


@dataclass
class ProcessResult:
    metadata: ProcessMetadata
    data: Optional[dict]


@dataclass
class RuleContext:
    infile: str
    local_yml: dict
    remote_yml: Optional[dict]
    final_yml: dict


class BitbucketCloudService:

    def get_repository_info(self, repository_path: str) -> dict:
        """
        Retrieves repository information
        :param repository_path: Repository path (ex: account/repo)
        :return: JSON response of repository information
        """
        request = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/")
        request.raise_for_status()
        request.close()
        return request.json()

    def get_template_yml(self, repository_path: str, version: str) -> dict:
        """
        Retrieves template yml
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: YAML object
        """
        request = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/bitbucket-pipelines.yml")
        request.raise_for_status()
        request.close()
        return request.text


class Check:

    def __init__(self, context: RuleContext):
        self.failures = Failures(criticals=[], warnings=[], skipped=[])
        self.context = context

    def get_failures(self):
        return self.failures

def validate(context: RuleContext) -> tuple:
    infile = context.infile

    # RuleEngine
    check = Check(context)

    return check.get_failures()


def extract_information(local_yml: dict, infile: str) -> RuleContext:
    bitbucket_api_service = BitbucketCloudService()

    local_yml_valid = LOCAL_METADATA.validate(local_yml)
    if not local_yml_valid:
        warnings.warn("Structure you are using, is deprecated. Please, update to current one", DeprecationWarning)

    final_yml = local_yml.copy()
    remote_yml = None

    repository_path = final_yml['repositoryPath']

    # Fetch Repository information
    if 'logo' not in final_yml:
        try:
            repo_info = bitbucket_api_service.get_repository_info(repository_path)
            final_yml['logo'] = repo_info['links']['avatar']['href']
        except HTTPError as error:
            logger.debug(error)

    # Fetch the bitbucket-pipeline.yml
    try:
        remote_yml = bitbucket_api_service.get_template_yml(repository_path, 'master')
        final_yml['yml'] = remote_yml
    except HTTPError as error:
        logger.debug(error)

    return RuleContext(infile=infile, local_yml=local_yml, remote_yml=remote_yml, final_yml=final_yml)


def process(infile: str) -> ProcessResult:
    failures = Failures(criticals=[], warnings=[], skipped = [])
    errors = []
    yaml_file = None

    with open(infile, 'r') as stream:
        try:
            manifest = yaml.safe_load(stream)
            context = extract_information(local_yml=manifest, infile=infile)
            yaml_file = context.final_yml
            failures = validate(context)
        except Exception as e:
            errors.append(str(e))

        print(
            f"Template: {infile}. "
            f"Criticals: {len(failures.criticals)} "
            f"Warnings: {len(failures.warnings)} "
            f"Errors: {len(errors)} "
            f"Skipped: {len(failures.skipped)}"
        )
        return ProcessResult(
            metadata=ProcessMetadata(
                file=infile,
                valid=(len(failures.criticals) == 0 and len(failures.warnings) == 0 and len(errors) == 0),
                failures=failures,
                errors=errors),
            data=yaml_file)


def extract_data(results: List[ProcessResult]) -> List[dict]:
    data: List[dict] = [result.data for result in results if result.data]
    return data


def extract_errors(results: List[ProcessResult]) -> List[ProcessResult]:
    errors: List[ProcessResult] = [x for x in results if not x.metadata.valid]
    return errors


def main():
    path = sys.argv[1] if len(sys.argv) > 1 else 'templates/*.yml'
    list_files = sorted(glob.glob(path))
    logger.info("Starting: " + str(len(list_files)) + " template(s) to verify.")

    t1 = datetime.now()

    with ThreadPoolExecutor() as executor:
        results = list(executor.map(process, list_files))

        errors = extract_errors(results)
        if errors:
            for error in errors:
                if error.metadata.failures.criticals:
                    logger.error(f"{asdict(error.metadata)}")
                else:
                    logger.warning(f"{asdict(error.metadata)}")

        data = extract_data(results)

    with open('templates.json', 'w', encoding='utf8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)

    t2 = datetime.now()
    logger.info("Finished. Time: " + str(t2 - t1))


if __name__ == '__main__':
    main()
